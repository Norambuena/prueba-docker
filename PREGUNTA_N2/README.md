Desarrollo de la segunda pregunta.


1.- Para construir la imagen usaremos el dockerfile de la primera pregunta, pero con un leve cambio ya que se elimina 
    le linea COPY ./index.html /usr/share/nginx/html/index.html,para este caso no es necesario.
    Ahora ejecutaremos el siguiente comando:
     
    docker build -t prueba_devops:0.0.2 .

2.- Ahora debemos desplegar el contenedor, con el siguiente comando:
    
    docker run -p 8080:80 -v ${PWD}/index.html:/usr/share/nginx/html/index.html prueba_devops:0.0.2

    En ese caso para poder editar fichero index.html, sin necesidad de recrear el contenedor, debemos 
    especificaremos el argumento -v con el que se creará un volumen con el fichero que le indiquemos y no
    copiar ficheros innecesarios.
