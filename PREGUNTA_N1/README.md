Desarrollo de la primera pregunta.

1.- Para proceder con la construcción de la imagen, ejecutaremos el siguiente comando:
     
    docker build -t prueba_devops:0.0.1 .

    en donde se especifica un tag para la imgan con el argumento -t

2.- Ahora debemos desplegar el contenedor, con el siguiente comando:
    
    docker run -p 8080:80 prueba_devops:0.0.1

    Es aquí donde debemos indicar el puerto con el cual podremos acceder al contenedor, 
    con el argumento -p, siendo 8080 el puerto a exponer y 80 el puerto del contenedor.
    