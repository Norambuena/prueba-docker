Detalle ejecución pregunta 3

1.- Desplegar contenedor Mysql. 

    docker run --name mysql_PruebaDevops -e MYSQL_ROOT_PASSWORD=PruevaDevops -p 3306:3306 -d mysql:5.7

    --name especificamos el nombre del contenedor
    -e (variable entorno), establecemos la contraseña del usuario root
    -p exponemos el puerto del contenedor para poder acceder a el
    -d lo ejecutamos en segundo plano

2.- Nos conectamos al contenedor, para poder crear el usuario.

    docker exec -it mysql_PruebaDevops mysql -uroot -p

2.1.- Después de habernos conectado al contenedor, creamos un usuario:
     creación: CREATE USER 'PruebaDevops'@'localhost' IDENTIFIED BY 'usuarioPruebaDevops';
     otorgar privilegios: GRANT ALL PRIVILEGES ON * . * TO 'PruebaDevops'@'localhost';
     guardar cambios: FLUSH PRIVILEGES;
    